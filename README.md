# TeamManager

Contains the API with implemented authorization by using keycloak.
keycloak and frontend is started and in use with docker.
### TeamManagerAPI
[TeamManagerAPI](https://gitlab.com/bluhill13/teammanagerapi/-/tree/master/TeamManagerAPI)

### Evidence
<details>
<summary>Image proof</summary>
<br>
![Login](./images/login.png "Login")
<br>
![Token](./images/token.png "token")
<br>
![Postman](./images/postman.png "Login")
</details>


