﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Models.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TeamManagerAPI.Model.Helper;

namespace TeamManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AthleteTeamController : ControllerBase
    {
        private readonly TeamManagerDbContext _context;
        private readonly ILogger<AthleteTeamController> _logger;
        private readonly IMethodNameService _methodNameService;
        public AthleteTeamController(TeamManagerDbContext context,ILogger<AthleteTeamController> logger, IMethodNameService methodNameService)
        {
            _context = context;
            _logger = logger;
            _methodNameService = methodNameService;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AthleteTeam>>> GetAll()
        {
            try
            {
                var result = await _context.athleteTeams.ToListAsync();
                if (result == null)
                {
                    _logger.LogError($"{_methodNameService.GetCallerName()}: There is no data in this table");
                    return NotFound();
                }
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"{_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpGet("{athleteId}/{teamId}")]
        public async Task<ActionResult<AthleteTeam>> GetById(int athleteId, int teamId)
        {
            try
            {
                var result = await _context.athleteTeams.FindAsync(athleteId, teamId);
                if (result == null)
                {
                    _logger.LogError($"{_methodNameService.GetCallerName()}: There is no data in this table");
                    return NotFound();
                }
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"{_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpGet("TforA/{id}")]
        public async Task<ActionResult<IEnumerable<AthleteTeam>>> GetAthleteTeamByAthleteId(int id)
        {
            try
            {
                var result = await _context.athleteTeams.Select(s => s).Where(s => s.AthleteId == id).ToListAsync();
                if (result == null)
                {
                    _logger.LogError($"{_methodNameService.GetCallerName()}: Athlete with id = {id} is not part of any teams");
                    return NotFound();
                }
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"{_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpGet("AforT/{id}")]
        public async Task<ActionResult<IEnumerable<AthleteTeam>>> GetAthleteTeamByTeamId(int id)
        {
            try
            {
                var result = await _context.athleteTeams.Select(s => s).Where(s => s.TeamId == id).ToListAsync();
                if (result == null)
                {
                    _logger.LogError($"{_methodNameService.GetCallerName()}: Athlete with id = {id} is not part of any teams");
                    return NotFound();
                }
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"{_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();

        }
        [Authorize]
        [HttpPost("{athleteId}/teamId")]
        public async Task<ActionResult<AthleteTeam>> AddAthleteToTeam(int athleteId, int teamId)
        {
            try
            {
                var athleteResult = await _context.Athletes.FindAsync(athleteId);
                var teamResult = await _context.Teams.FindAsync(teamId);
                if (athleteResult == null || teamResult == null)
                {
                    _logger.LogError($"{_methodNameService.GetCallerName()}: Athlete with id = {athleteId} or team with Id: {teamId} does not exist");
                    return NotFound();
                }
                var exists = _context.athleteTeams.Select(s => s).Where(s => s.AthleteId == athleteId && s.TeamId == teamId);
                if (exists != null)
                {
                    AthleteTeam a = new AthleteTeam();
                    a.AthleteId = athleteId;
                    a.TeamId = teamId;
                    _context.athleteTeams.Add(a);
                    _context.SaveChanges();
                    _logger.LogInformation($"{_methodNameService.GetCallerName()}: Athlete added to team!");
                    return await _context.athleteTeams.FindAsync(athleteId, teamId);
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }

            return BadRequest();
        }
        [Authorize]
        [HttpDelete("{athleteId}/{teamId}")]
        public async Task<ActionResult<Team>> DeleteAthleteTeam(int athleteId, int teamId)
        {
            try
            {
                var result = await _context.Teams.FindAsync(athleteId, teamId);
                if (result == null)
                {
                    _logger.LogError($"{_methodNameService.GetCallerName()}: Team/Athlete relationship does not exist in database");
                    return NotFound();
                }
                _context.Teams.Remove(result);
                await _context.SaveChangesAsync();
                _logger.LogInformation($"{_methodNameService.GetCallerName()}: Athlete/Team relationship deleted!");
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"{_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
    }
}
