﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entity_Framework_Migrations.Models.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TeamManagerAPI.Model.DTOs;
using TeamManagerAPI.Model.DTOs.AthleteDtos;
using TeamManagerAPI.Model.Helper;

namespace TeamManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AthleteController : ControllerBase
    {
        private readonly TeamManagerDbContext _context;
        private readonly ILogger<AthleteController> _logger;
        private readonly IMethodNameService _methodNameService;
        private readonly IMapper _mapper;
        public AthleteController(
            TeamManagerDbContext context,
            ILogger<AthleteController> logger,
            IMethodNameService methodNameService,
            IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _methodNameService = methodNameService;
            _mapper = mapper;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AthleteMultiDto>>> GetAll()
        {
            try
            {
                List<AthleteMultiDto> athleteMultiDtos = new List<AthleteMultiDto>();
                var result = await _context.Athletes.ToListAsync();
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: There is no data in this table");
                    return NotFound();
                }
                foreach (Athlete a in result)
                {
                    athleteMultiDtos.Add(_mapper.Map<AthleteMultiDto>(a));
                }
                return athleteMultiDtos;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<AthleteSingleDto>> GetAthleteById(int id)
        {
            try
            {
                
                var result = await _context.Athletes.Where(s => s.Id == id).Include(s => s.Coach).Include(s=>s.athleteTeam).ThenInclude(s=>s.Team).ToListAsync();
                if (result.Count == 0)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Athlete with id = {id} does not exist in database");
                    return NotFound();
                }
                return _mapper.Map<AthleteSingleDto>(result.First());
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<AthleteCreateDto>> CreateAthlete(AthleteCreateDto athleteDto)
        {
            try
            {
                await _context.Athletes.AddAsync(_mapper.Map<Athlete>(athleteDto));
                _context.SaveChanges();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Athlete created!");
                return athleteDto;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }

            return BadRequest();
        }
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult<AthleteUpdateDto>> UpdateAthlete(int id, AthleteUpdateDto athleteDto)
        {
            try
            {
                if (id != athleteDto.Id)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Mismatch between id and object id!");
                    return BadRequest();
                }
                _context.Entry(_mapper.Map<Athlete>(athleteDto)).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Athlete with {nameof(athleteDto.Id)}: {id} updated!");
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            

            return BadRequest();
        }
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Athlete>> DeleteAthlete(int id)
        {
            try
            {
                var result = await _context.Athletes.FindAsync(id);
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Athlete with {nameof(result.Id)}: = {id} does not exist in database");
                    return NotFound();
                }
                _context.Athletes.Remove(result);
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Athlete with {nameof(result.Id)}: {id} deleted!");
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
    }
}
