﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Models.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TeamManagerAPI.Model.DTOs.CoachDtos;
using TeamManagerAPI.Model.Helper;

namespace TeamManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachController : ControllerBase
    {
        private readonly TeamManagerDbContext _context;
        private readonly ILogger<CoachController> _logger;
        private readonly IMethodNameService _methodNameService;
        private readonly IMapper _mapper;
        public CoachController(
            TeamManagerDbContext context,
            ILogger<CoachController> logger,
            IMethodNameService methodNameService,
            IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _methodNameService = methodNameService;
            _mapper = mapper;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachMultiDto>>> GetAll()

        {
            try
            {
                List<CoachMultiDto> coachMultiDtos = new List<CoachMultiDto>();
                var result = await _context.Coaches.Include(s => s.Team).ToListAsync();
                if (result.Count == 0)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: There is no data in this table");
                    return NotFound();
                }
                foreach (Coach c in result)
                {
                    coachMultiDtos.Add(_mapper.Map<CoachMultiDto>(c));
                }
                return coachMultiDtos;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<CoachSingleDto>> GetCoachById(int id)
        {
            try
            {
                var result = await _context.Coaches.Include(s => s.Team).Include(s => s.athletes).Where(s => s.Id == id).ToListAsync();
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Coach with Id = {id} does not exist in database");
                    return NotFound();
                }
                return _mapper.Map<CoachSingleDto>(result.First());
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<CoachCreateDto>> CreateCoach(CoachCreateDto coachCreateDto)
        {
            try
            {
                await _context.Coaches.AddAsync(_mapper.Map<Coach>(coachCreateDto));
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Coach created!");
                return coachCreateDto;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }

            return BadRequest();
        }
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult<CoachUpdateDto>> UpdateCoach(int id, CoachUpdateDto coachUpdateDto)
        {
            try
            {
                if (id != coachUpdateDto.Id)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Mismatch between id and object id!");
                    return BadRequest();
                }

                _context.Entry(_mapper.Map<Coach>(coachUpdateDto)).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Coach with {nameof(coachUpdateDto.Id)}: {id} updated!");
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Coach>> DeleteCoach(int id)
        {
            try
            {
                var result = _context.Coaches.Find(id);
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Coach with id = {id} does not exist in database");
                    return NotFound();
                }
                _context.Coaches.Remove(result);
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Coach with Id: {id} deleted!");
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
    }
}
