﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Models.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TeamManagerAPI.Model.DTOs.TeamDtos;
using TeamManagerAPI.Model.Helper;

namespace TeamManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamManagerDbContext _context;
        private readonly ILogger<TeamController> _logger;
        private readonly IMethodNameService _methodNameService;
        private readonly IMapper _mapper;
        public TeamController(
            TeamManagerDbContext context,
            ILogger<TeamController> logger,
            IMethodNameService methodNameService,
            IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _methodNameService = methodNameService;
            _mapper = mapper;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamMultiDto>>> GetAll()
        {
            try
            {
                List<TeamMultiDto> teamMultiDtos = new List<TeamMultiDto>();
                var result = await _context.Teams.ToListAsync();
                
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: There is no data in this table");
                    return NotFound();
                }
                foreach (Team t in result)
                {
                    teamMultiDtos.Add(_mapper.Map<TeamMultiDto>(t));
                }
                return teamMultiDtos;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamSingleDto>> GetTeamById(int id)
        {
            try
            {
                var result = await _context.Teams.Where(s=>s.Id == id).Include(s=>s.athleteTeam).ThenInclude(s=>s.Athlete).ToListAsync();
                var coach = await _context.Coaches.Where(s => s.TeamId == id).ToListAsync();
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Team with id = {id} does not exist in database");
                    return NotFound();
                }
                TeamSingleDto dto = _mapper.Map<TeamSingleDto>(result);
                dto.CoachName = coach.First().ToString();
                return dto;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<TeamCreateDto>> CreateTeam(TeamCreateDto teamCreateDto)
        {
            try
            {
                await _context.Teams.AddAsync(_mapper.Map<Team>(teamCreateDto));
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Team created!");
                return teamCreateDto;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }

            return BadRequest();
        }
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult<TeamUpdateDto>> UpdateTeam(int id, TeamUpdateDto teamUpdateDto)
        {
            try
            {
                if (id != teamUpdateDto.Id)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Mismatch between id and object id!");
                    return BadRequest();
                }
                _context.Entry(_mapper.Map<Team>(teamUpdateDto)).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Team with Id: {id} updated!");
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Team>> DeleteTeam(int id)
        {
            try
            {
                var result = await _context.Teams.FindAsync(id);
                if (result == null)
                {
                    _logger.LogError($"Method={_methodNameService.GetCallerName()}: Team with id = {id} does not exist in database");
                    return NotFound();
                }
                _context.Teams.Remove(result);
                _context.SaveChanges();
                _logger.LogInformation($"Method={_methodNameService.GetCallerName()}: Team with Id: {id} deleted!");
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"Method={_methodNameService.GetCallerName()}: Something went wrong! {e}");
            }
            return BadRequest();
        }
    }
}
