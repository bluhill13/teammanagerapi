﻿using System.Runtime.CompilerServices;

namespace TeamManagerAPI.Model.Helper
{
    public interface IMethodNameService
    {
        string GetCallerName([CallerMemberName] string caller = null);
    }
}