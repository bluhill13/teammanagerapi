﻿using AutoMapper;
using Entity_Framework_Migrations.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamManagerAPI.Model.DTOs;
using TeamManagerAPI.Model.DTOs.AthleteDtos;

namespace TeamManagerAPI.Model.Profiles
{
    public class AthleteProfile : Profile
    {
        public AthleteProfile()
        {
            CreateMap<Athlete, AthleteSingleDto>().ForMember(aDto=> aDto.CoachName, a => a.MapFrom(a => a.Coach.ToString()))
                                            .ForMember(aDto => aDto.Teams, a => a.MapFrom(a => a.athleteTeam.Where(at => at.AthleteId == a.Id).Select(at => at.Team.Team_name)));
            CreateMap<Athlete, AthleteMultiDto>();
            CreateMap<AthleteUpdateDto, Athlete>();
            CreateMap<AthleteCreateDto, Athlete>();
        }
    }
}
