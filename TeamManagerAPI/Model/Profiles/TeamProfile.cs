﻿using AutoMapper;
using Entity_Framework_Migrations.Models.Data;
using System.Linq;
using TeamManagerAPI.Model.DTOs.TeamDtos;

namespace TeamManagerAPI.Model.Profiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamSingleDto>().ForMember(tDto => tDto.AthleteNames, t => t.MapFrom(t=>t.athleteTeam.Where(s =>s.TeamId == t.Id).Select(s=>s.Athlete.ToString())));
            CreateMap<Team, TeamMultiDto>();

            CreateMap<TeamCreateDto, Team>();
            CreateMap<TeamUpdateDto, Team>();
        }
    }
}
