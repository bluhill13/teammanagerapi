﻿using AutoMapper;
using Entity_Framework_Migrations.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamManagerAPI.Model.DTOs;
using TeamManagerAPI.Model.DTOs.CoachDtos;

namespace TeamManagerAPI.Model.Profiles
{
    public class CoachProfile : Profile
    {
        public CoachProfile()
        {
            CreateMap<Coach, CoachSingleDto>().ForMember(cDto => cDto.AthleteNames, c => c.MapFrom(c => c.athletes.Select(s=>s.ToString())))
                                            .ForMember(cDto => cDto.TeamName, c => c.MapFrom(c => c.Team.Team_name));

            CreateMap<Coach, CoachMultiDto>().ForMember(cDto => cDto.TeamName, c => c.MapFrom(c => c.Team.Team_name));

            CreateMap<CoachUpdateDto, Coach>();
            CreateMap<CoachCreateDto, Coach>();
        }
    }
}
