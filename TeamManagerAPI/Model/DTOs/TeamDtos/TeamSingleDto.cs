﻿
namespace TeamManagerAPI.Model.DTOs.TeamDtos
{
    public class TeamSingleDto
    {
        public int Id { get; set; }
        public string Team_name { get; set; }
        public string Sport { get; set; }
        public string CoachName { get; set; }
        public string [] AthleteNames { get; set; } 
    }
}
