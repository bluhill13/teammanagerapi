﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamManagerAPI.Model.DTOs.TeamDtos
{
    public class TeamMultiDto
    {
        public int Id { get; set; }
        public string Team_name { get; set; }
        public string Sport { get; set; }
        public string CoachName { get; set; }
    }
}
