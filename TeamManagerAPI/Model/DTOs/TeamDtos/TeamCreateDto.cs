﻿
namespace TeamManagerAPI.Model.DTOs.TeamDtos
{
    public class TeamCreateDto
    {
        public string Team_name { get; set; }
        public string Sport { get; set; }
    }
}
