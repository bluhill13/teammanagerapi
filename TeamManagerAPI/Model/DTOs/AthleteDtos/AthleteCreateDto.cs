﻿
namespace TeamManagerAPI.Model.DTOs.AthleteDtos
{
    public class AthleteCreateDto
    {
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public int? Age { get; set; }
    }
}
