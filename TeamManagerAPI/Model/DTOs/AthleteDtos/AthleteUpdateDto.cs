﻿
namespace TeamManagerAPI.Model.DTOs.AthleteDtos
{
    public class AthleteUpdateDto
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public int Age { get; set; }
        public int CoachId { get; set; }
    }
}
