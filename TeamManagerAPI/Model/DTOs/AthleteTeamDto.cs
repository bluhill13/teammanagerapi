﻿
namespace TeamManagerAPI.Model.DTOs
{
    public class AthleteTeamDto
    {
        public int AthleteId { get; set; }
        public int TeamId { get; set; }
    }
}
