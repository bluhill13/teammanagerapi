﻿
namespace TeamManagerAPI.Model.DTOs.CoachDtos
{
    public class CoachCreateDto
    {
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Sport { get; set; }
        public int Age { get; set; }
    }
}
