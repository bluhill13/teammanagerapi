﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamManagerAPI.Model.DTOs.CoachDtos
{
    public class CoachMultiDto
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Sport { get; set; }
        public int Age { get; set; }
        public string TeamName { get; set; }
    }
}
