﻿
namespace TeamManagerAPI.Model.DTOs.CoachDtos
{
    public class CoachSingleDto
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Sport { get; set; }
        public int Age { get; set; }
        public string TeamName { get; set; }
        public string [] AthleteNames { get; set; }
    }
}
