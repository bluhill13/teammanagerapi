﻿using System.Collections.Generic;

namespace Entity_Framework_Migrations.Models.Data
{
    public class Coach
    {
        public int Id { get; set; }
        public string First_name { get; set; } = "unknown";
        public string Last_name { get; set; } = "unknown";
        public string? Sport { get; set; } = "unknown";
        public int? Age { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        //Generates the one to many relationship for athletes
        public ICollection<Athlete> athletes { get; set; }
        /// <summary>
        /// Helps putting first and last name together
        /// </summary>
        /// <returns>First and last name</returns>
        public override string ToString()
        {
            return First_name + " " + Last_name;
        }
    }
}
