﻿using System.Collections.Generic;

namespace Entity_Framework_Migrations.Models.Data
{
    public class Team
    {
        public int Id { get; set; }
        public string Team_name { get; set; } = "unknown";
        public string? Sport { get; set; } = "unknown";
        public ICollection<AthleteTeam> athleteTeam { get; set; }
    }
}
