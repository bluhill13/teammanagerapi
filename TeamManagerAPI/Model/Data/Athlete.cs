﻿using System.Collections.Generic;

namespace Entity_Framework_Migrations.Models.Data
{
    public class Athlete
    {
        public int Id { get; set; }
        public string First_name { get; set; } = "unknown";
        public string Last_name { get; set; } = "unknown";
        public int? Age { get; set; } = 0;
        public int? CoachId { get; set; }
        public Coach Coach { get; set; }
        public ICollection<AthleteTeam> athleteTeam { get; set; }
        /// <summary>
        /// Helps putting first and last name together
        /// </summary>
        /// <returns>First and last name</returns>
        public override string ToString()
        {
            return First_name + " " + Last_name;
        }

    }
}
