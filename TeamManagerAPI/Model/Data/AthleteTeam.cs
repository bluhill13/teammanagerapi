﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity_Framework_Migrations.Models.Data
{
    /// <summary>
    /// Table creating a many to many relationship between teams and athletes
    /// </summary>
    public class AthleteTeam
    {
        public int AthleteId { get; set; }
        public Athlete Athlete { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }

    }
}
