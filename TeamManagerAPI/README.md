# TeamManagerAPI

Wep API, with ef core.

### TeamManger 1.0 (Commit: 644bd794a5f963d71a0f8b91459b1e364f6b2ed8)

### TeamManager 2.0(Commit: a314b737437e2949247f0d77000fabbae35e892d)
ChangeLog: implemented data transfer object classes with use of Automapper
### TeamManager 3.0 (Commit: bcaeaa09d4c96ec20f78447ceb5ddf3edebebd1f)
Changelog: Implemented async for all methods in controllers
### TeamManager 4.0 (Commit: 06e520bd7ed226325bfc3512e8ddc60c82eb10d1)
Changelog: Implemented authorization with OAuth 2.0, added Tokensecrets for connection to OAuth to user-secrets

## Running program
To see logging run with powershell and use postman with localhost:5000
1. open powershell in repository folder (shift + rightclick)
2. Then write ```dotnet run``` 



